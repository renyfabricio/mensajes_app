/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.util.Scanner;

/**
 *
 * @author renyfabricio
 */
public class mensajeService {
    public static void crearMensaje(){
        //Crear Objeto
        Scanner sc=new Scanner(System.in);
        System.out.println("Escribe Mensaje");
        String mensaje=sc.nextLine();
        
        System.out.println("Autor");
        String nombre=sc.nextLine();
        
        Mensajes registro =new Mensajes();
            registro.setMensaje(mensaje);
            registro.setAutor_mensaje(nombre);
        
        MensajesDAO.CrearMensajeDB(registro);
   }
    public static void listarMensaje(){
        MensajesDAO.leerMensajeDB();
        
    }
    public static void editarMensaje(){
        Scanner sc=new Scanner(System.in);
        System.out.println("Escriba el nuevo mensaje");
        String mensaje=sc.nextLine();
        System.out.println("Indica el ID del mensaje a Editar");
        int id_mensaje=sc.nextInt();
        Mensajes actualizacion =new Mensajes();
        actualizacion.setId_mensaje(id_mensaje);
        actualizacion.setMensaje(mensaje);
        MensajesDAO.actualizarMensajeDB(actualizacion);
        
        
    }
    public static void borrarMensaje(){
        Scanner sc=new Scanner(System.in);
        System.err.println("Indica el ID del mensaje a borrar");
        int id_mensaje=sc.nextInt();
        MensajesDAO.borrarMensajeDB(id_mensaje);
        
    }
}

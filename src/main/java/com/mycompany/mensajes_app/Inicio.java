/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.util.Scanner;

/**
 *
 * @author renyfabricio
 */
public class Inicio {
    public static void main (String [] args){
        
        //Crear Menu Mensajes
        //Crear Objeto Scanner
        
        Scanner sc=new Scanner(System.in);
        int opcion=0;
        do{
            System.out.println("--------------------");
            System.out.println("Aplicación de Mensajes");
            System.out.println("1.- Crear Mensaje");
            System.out.println("2.- Listar Mensajes");
            System.out.println("3.- Actualizacion Mensaje");
            System.out.println("4.- Eliminar Mensaje");
            System.out.println("5.- Salir");
            //Leer opcion del usuario
            opcion=sc.nextInt();
            switch(opcion){
                case 1:
                    mensajeService.crearMensaje();
                    break;
                case 2:
                    mensajeService.listarMensaje();
                    break;
                case 3:
                    mensajeService.editarMensaje();
                    break;
                case 4:
                    mensajeService.borrarMensaje();
                    break;
                default:
                    break;
            }
          }while(opcion!=5);
      
    }
    
}
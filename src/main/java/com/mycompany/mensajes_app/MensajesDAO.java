/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author renyfabricio
 */
public class MensajesDAO {
    
    public static void CrearMensajeDB(Mensajes mensaje){
        Conexion db_connect=new Conexion();
        
        try (Connection conexion=db_connect.get_connection()){
            PreparedStatement ps=null;
          
            try {
                //String query="INSERT INTO mensajes (mensaje, autor_mensaje) VALUES (?, ?);";
                String query="INSERT INTO mensajes (mensaje, autor_mensaje) VALUES (?, ?);";
                ps=conexion.prepareStatement(query);
                ps.setString(1, mensaje.mensaje);
                ps.setString(2, mensaje.autor_mensaje);
                ps.executeUpdate();
                System.out.println("----Mensaje Creado Exitosamente --------");
            } catch (Exception ex) {
                System.out.println(ex);
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
       }
    public static void leerMensajeDB(){
        
        Conexion db_connect=new Conexion();
         try (Connection conexion=db_connect.get_connection()){
                PreparedStatement ps=null;
                
                ResultSet rs=null;
                
                String query="SELECT * FROM mensajes";
                
                ps=conexion.prepareStatement(query);
                rs=ps.executeQuery();
                
                while(rs.next()){
                    System.out.println("ID"+rs.getInt("id_mensaje"));
                    System.out.println("Mensaje: "+rs.getString("mensaje"));
                    System.out.println("Autor: "+rs.getString("autor_mensaje"));
                    System.out.println("Fecha: "+rs.getString("fecha_mensaje"));
                    System.out.println("");
                    
                }
                
            
           } catch (SQLException ex) {
               System.out.println("No se puedo traer mensajes");
                System.out.println(ex);
          }
        
    }
    public static void borrarMensajeDB(int id_mensaje){
         Conexion db_connect=new Conexion();
          try (Connection conexion=db_connect.get_connection()){
            PreparedStatement ps=null;
              try {
                  String query="delete from mensajes where id_mensaje=?";
                  ps=conexion.prepareStatement(query);
                  ps.setInt(1, id_mensaje);
                  ps.executeUpdate();
                  System.out.println("El mensaje ha sido borrado");
                  
                  
              } catch (Exception e) {
                  System.out.println(e);
                  System.out.println("No se pudo borrar el mensaje");
              }
         } catch (Exception ex) {
                System.out.println(ex);
            }
    }
    public static void actualizarMensajeDB(Mensajes mensaje){
         Conexion db_connect=new Conexion();
          try (Connection conexion=db_connect.get_connection()){
            PreparedStatement ps=null;
              try {
                  String query="update mensajes set mensaje=? where id_mensaje=?";
                  ps=conexion.prepareStatement(query);
                  ps.setString(1, mensaje.getMensaje());
                  ps.setInt(2, mensaje.getId_mensaje());
                  ps.executeUpdate();
                  System.err.println("Se actualizo el Mensaje");
                  
                  
              } catch (Exception e) {
                  System.out.println("No se actualizo mensaje");
                  System.out.println(e);
              }
            } catch (Exception ex) {
                System.out.println(ex);
            } 
    }
}

